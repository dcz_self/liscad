tolerance=0.2;
angle=$t * 140;

// static ring
color("red")
difference() {
    union() {
        // basic shape
        rotate_extrude($fn=100)
        translate([10, 0])
        difference() {
            square([6, 3]);
            // cutout
            translate([1, 0])
            square([4, 2]);
        };
        // support the moving ring
        // limited angle, otherwise will collide with the ramp
        rotate(130, [0, 0, 1])
        rotate_extrude(angle=90, $fn=100)
        translate([10, 0])
        translate([0, 1])
        square([3 - tolerance, 1]);

        // free end guide
        rotate(-90, [0, 0, 1])
        difference() {
            // pipe
            translate([14.5, 0])
            rotate(-60, [1, 0, 0])
            translate([0, 0, -10])
            cylinder(20, r=2, $fn=20);

            // cutout again
            rotate_extrude()
            translate([11, 0])
            {
                square([4, 2]);
                translate([0, -10])
                square([10, 10]);
            }
        }
    }
    // wire end hole
    rotate(-120, [0, 0, 1]) // match the entry in the other ring
    translate([11.5, 0])
    rotate(60, [1, 0, 0])
    {
        cylinder(100, r=0.5, $fn=10);
        translate([0, 0, 6])
        cylinder(10, r=1, $fn=10);
    }
    // free end hole
    rotate(-90, [0, 0, 1])
    translate([14.5, 0])
    rotate(-60, [1, 0, 0])
    cylinder(11, r=0.5, $fn=20);
}

// moving ring
rotate(angle, [0, 0, 1])
color("cyan")
translate([0, 0, -10]) // clarity of view
mirror([0, 0, 1]) // to set 0 in between the rings
{
    // solid part
    rotate_extrude()
    translate([10, 0]) {
        // grip support
        square([1, 10]);
        // protector
        polygon(points=[[0,0], [0,3], [3,3], [6,0]]);
        // debug: grip support extension
        *translate([0, -2])
        square([1, 10]);
    }
    // wire support
    rotate_extrude(angle=260)
    translate([10, 0])
    translate([3, -2]) {
        // ramp
        square([1, 2]);
    }
    // wire guide - it blocks access to free hole, so needs to end within its movement range
    rotate_extrude(angle=120)
    translate([10, 0])
    translate([3, -2]) {
        square([2 - tolerance, 1]);
    }
    // wire ramp
    translate([1, 2, 0])
    rotate(-65, [0, 0, 1])
    rotate_extrude(angle=60)
    translate([9, 0])
    translate([3, -2]) {
        // guide
        square([1, 2]);
        // ramp
        square([2 - tolerance, 1]);
    }
}

