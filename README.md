LISCAD
====

OpenSCAD doesn't let you do macros. LISP does.

Useful for: keeping negative geometry together with positive in one call, e.g.:

```
my_piece {
    base_hull();
    translate([10, 0]);
    negative_part();
    positive_part();
}
```

Imagine this translated to sth like:

```
difference() {
    union() {
        base_hull();
        translate([10, 0])
        positive_part();
    };
    translate([10, 0])
    negative_part();
}
```

Syntax
-------

LISP-like.

```
'(tolerance=0.2)
'(angle=$t*140)

;; static ring
(color '("red")
 (difference '()
    (union '()
        ;; basic shape
        (rotate_extrude '($fn=100)
         (translate '([10, 0])
          (difference '()
            (square '([6, 3]))
            ;; cutout
            (translate '([1, 0])
             (square '([4, 2]))
            )
        )))
)))
```

Macro definition
--------------

```
(defmacro newsq (w h) (square '([w h])))
(newsq 0 1)
;; evaluates to (square '([0 1]))
```

Usage
-------

```
python3 converter.py file.secd > file.scad
```

License
---------

AGPLv3
