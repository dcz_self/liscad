import sexpdata

'''Parses S-expressions into OpenSCAD source code.
Can be used to add macros.
Usage: modify sexp code, add a file change waiter, update scad code. OpenSCAD picks up and renders changes.

Not tested.
'''

def replace_symbols(tree, syms):
    if isinstance(tree, sexpdata.Symbol):
        return syms.get(tree.value(), tree)
    if isinstance(tree, sexpdata.Bracket):
        return sexpdata.Bracket([replace_symbols(i, syms) for i in tree.value()], tree._bra)
    if isinstance(tree, sexpdata.Quoted):
        return sexpdata.Quoted([replace_symbols(i, syms) for i in tree.value()])
    if isinstance(tree, list):
        return [replace_symbols(i, syms) for i in tree]
    else:
        return tree

def defmacro(params, definition):
    """
    No quoting, deal with it.
    (defmacro mymacro (a b) (square '([a b])))
    """
    param_names = [p.value() for p in params]
    def f(args):
        return replace_symbols(definition, dict(zip(param_names, args)))
    return f

def unquote(q):
    def unp(a):
        if isinstance(a, sexpdata.Symbol):
            return a.value()
        return repr(a)
    return (unp(i) for i in q.value())

def indent(v):
    return ''.join('    ' + l + '\n' for l in v.splitlines())

def unfurl(c, item):
    if isinstance(item, sexpdata.Quoted):
        def unp(a):
            if isinstance(a, sexpdata.Symbol):
                return a.value()
            elif isinstance(a, sexpdata.Bracket):
                return '[' + ', '.join(unp(i) for i in a.value()) + ']'
            return repr(a)
        return (unp(i) for i in item.value())
    elif isinstance(item, list):
        name = item[0].value()
        if name == 'defmacro':
            c[item[1].value()] = defmacro(item[2], item[3])
            return ''
        elif name in c:
            return unfurl(c, c[name](item[1:]))
        else:
            params = ', '.join(unfurl(c, item[1]))
            if len(item) == 2:
                children = ''
            elif len(item) == 3:
                children = '\n' + unfurl(c, item[2])
            else:
                children = ' {{\n{}\n}}'.format('\n'.join(indent(unfurl(c, i) + ';') for i in item[2:]))
            return '{}({}){}'.format(name, params, children)

def convert(s):
    data = sexpdata.loads('(\n' + s + '\n)')
    ctx = {}
    for item in data:
        if isinstance(item, sexpdata.Quoted):
            yield ', '.join(unquote(item)) + ';'
        else:
            yield unfurl(ctx, item) + ';'

if __name__ == '__main__':
    import sys
    for i in convert(open(sys.argv[1]).read()):
        print(i)
